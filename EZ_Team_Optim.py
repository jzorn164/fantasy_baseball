#import

import pandas as pd
import pulp

players = pd.read_csv('Export.csv')

projection_categories = ['Projected','Ceiling','Floor','ISO','wOBA']
df_con = pd.DataFrame({'Name':['New'],'Position':['Lineup'],'Team':['Was'],'GameTime':['GameTime'],'Salary':['Created'],'Score':['Here']})
print(players.dtypes)
print(players.head())

players = players[(players['GameTime'] > 0.65)]

teams = []
for team in players['Team'].unique():
	teams.append(team)

teams.append("None")

print(teams)

for j in range(len(teams)):

	con_team = teams[j]
	print(con_team)
	
	for category in range(0,5):

		projection_cat = projection_categories[category]
		lp = pulp.LpProblem('FantasyDraft',pulp.LpMaximize)

		lp_vars = [pulp.LpVariable('x_{0:04d}'.format(index), cat='Binary') for index in players.index]

		total_points = pulp.LpAffineExpression(zip(lp_vars,players[projection_cat]))
		total_cost = pulp.LpAffineExpression(zip(lp_vars,players['Salary']))

		lp += total_points
		lp += (total_cost <= 50000)

		p_con = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position']=='P')))
		c_con = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position']=='C')))
		b1_con = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position']=='1B')))
		b2_con = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position']=='2B')))
		b3_con = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position']=='3B')))
		ss_con = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position']=='SS')))
		of_con = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position']=='OF')))
		p_sal_con = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position']=='P')*(players['Salary'])))
		num_players = pulp.LpAffineExpression(zip(lp_vars, 1*(players['Position'] != "")))

		for team in players['Team'].unique():
			if team == con_team:
				lp += pulp.LpAffineExpression(zip(lp_vars, 1 * (players['Team'] == team)*(players['Position']!= 'P'))) >= 4, 'Team {} Constraint'.format(team)
			else:
				lp += pulp.LpAffineExpression(zip(lp_vars, 1 * (players['Team'] == team)*(players['Position']!= 'P'))) <= 2, 'Team {} Constraint'.format(team)
			
		lp += (p_con == 2)
		lp += (c_con == 1)
		lp += (b1_con == 1)
		lp += (b2_con == 1)
		lp += (ss_con == 1)
		lp += (b2_con == 1)
		lp += (of_con == 3)
		lp += (p_sal_con <= 20000)
		lp += (p_sal_con >= 16000)
		lp += (num_players == 10)
		
		lp.writeLP('MyProb.lp')
		lp.solve()

		include = [v.varValue for v in lp.variables()]
		players['include'] = include

		my_team = players[players['include'] == 1 ][['Name','Position','Team','GameTime','Salary',projection_cat]]
		#print(my_team.head(10))
		
		df_con = pd.DataFrame({'Name':[con_team],'Position':['Lineup'],'Team':['Stacked'],'GameTime':['GameTime'],'Salary':['4+'],'Score':[projection_cat]})
		
		my_new_team = my_team
		my_new_team.columns = ['Name','Position','Team','GameTime','Salary','Score']
		#print(my_new_team.head())
		
		if j == 0 and category == 0:
			all_my_teams = my_new_team
		#	print(all_my_teams.head(10))
		else:
			all_my_teams =pd.concat([all_my_teams,df_con,my_new_team])
			#print(all_my_teams)

all_my_teams.to_csv('LineupExport.csv')
print('The lineups were printed to a csv named "LineupExport.csv" in this directory')
